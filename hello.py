import json
import boto3
from flask import Flask
from flask import render_template
from services import root_dir

app = Flask(__name__)

def upload(event, context):
    print('some cat was uploaded to S3!!')

with open("{}/database/dogs.json".format(root_dir()), "r") as f:
    dogs  = json.load(f)

@app.route('/')
def index():
    return "Hello, world!", 200

@app.route('/dog')
def dog():
    dogs_list = []
    for dog in dogs['dogs']:
        dogs_list.append( dog['dog_url'] )
    return render_template('dog.html',dogs_list=dogs_list)

@app.route('/cat')
def cat():
    cats_list = []
    s3 = boto3.resource('s3')
    my_bucket = s3.Bucket('zappa-test-cat')
    for file in my_bucket.objects.all():
        cats_list.append (file.key)
    return render_template('cat.html',cats_list=cats_list)

# We only need this for local development.
if __name__ == '__main__':
    app.run()
